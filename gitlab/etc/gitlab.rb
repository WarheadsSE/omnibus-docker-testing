external_url 'http://gitlab.127.0.0.1.nip.io'
registry_external_url 'http://registry.127.0.0.1.nip.io'
gitlab_rails['gitlab_shell_ssh_port']=2222

puma['worker_processes'] = 0

sidekiq['max_concurrency'] = 10

prometheus_monitoring['enable'] = false

gitlab_rails['env'] = {
  'MALLOC_CONF' => 'dirty_decay_ms:1000,muzzy_decay_ms:1000'
}
# can't use cgrups inside Docker
# gitaly['cgroups_count'] = 2
# gitaly['cgroups_mountpoint'] = '/sys/fs/cgroup'
# gitaly['cgroups_hierarchy_root'] = 'gitaly'
# gitaly['cgroups_memory_enabled'] = true
# gitaly['cgroups_memory_limit'] = 250000
# gitaly['cgroups_cpu_enabled'] = true
# gitaly['cgroups_cpu_shares'] = 512

gitaly['concurrency'] = [
  {
    'rpc' => "/gitaly.SmartHTTPService/PostReceivePack",
    'max_per_repo' => 3
  }, {
    'rpc' => "/gitaly.SSHService/SSHUploadPack",
    'max_per_repo' => 3
  }
]
gitaly['env'] = {
  'LD_PRELOAD' => '/opt/gitlab/embedded/lib/libjemalloc.so',
  'MALLOC_CONF' => 'dirty_decay_ms:1000,muzzy_decay_ms:1000',
  'GITALY_COMMAND_SPAWN_MAX_PARALLEL' => '2'
}