#!/bin/bash
docker rm gldemo
docker run --name gldemo --rm -ti \
  -v ${PWD}/gitlab/etc:/etc/gitlab:z \
  -v ${PWD}/gitlab/log:/var/log/gitlab:z \
  -v ${PWD}/gitlab/opt:/var/opt/gitlab:z \
  -p 127.0.0.1:80:80/tcp \
  -p 127.0.0.1:2222:22/tcp \
  --memory 3g \
  --memory-swap 4g \
  gitlab/gitlab-ee:13.11.4-ee.0
