#!/bin/bash

docker run -it --network host -e ACCESS_TOKEN=$(cat gpt/access_token) \
  -v $PWD/gpt/config:/config -v $PWD/gpt/results:/results \
  gitlab/gitlab-performance-tool \
    --environment local.json \
    --options 20s_20rps.json \
    --tests \
      api_v4_groups_group_subgroups.js \
      api_v4_groups_group \
      api_v4_groups_issues \
      api_v4_groups_merge_requests \
      api_v4_groups_merge_requests \
      api_v4_groups
