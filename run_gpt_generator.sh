#!/bin/bash

docker run -it --network host -e ACCESS_TOKEN=$(cat gpt/access_token) \
  -v $PWD/gpt/config:/config -v $PWD/gpt/results:/results \
  gitlab/gpt-data-generator \
    --environment local.json \
    --large-project-tarball="https://gitlab.com/gitlab-org/quality/performance-data/raw/master/projects_export/small-project_13.0.0.tar.gz"
